# Foxtrot
<!--Status: ![](https://github.com/dahlia-os/pangolin-desktop/workflows/CI/badge.svg) ![](https://github.com/dahlia-os/pangolin-desktop/workflows/Deploy/badge.svg) ![gitlocalized ](https://gitlocalize.com/repo/5170/whole_project/badge.svg)

Latest APK: [download from gitlab](https://gitlab.com/dahlia-os/pangolin-desktop/-/jobs/artifacts/master/browse/build/app/outputs/apk/debug/?job=build_android)-->

Foxtrot is a (currently experimental) new experience with the following features:
* A swipe gesture from the bottom opens a universal menu.
* Apps get the whole display.
* Built-in apps and features are merged from Pangolin (not rebased, for good measure).

Please note that Foxtrot is very much a work in progress, and as such it is not currently bundled with dahliaOS, nor is it recommended to use it yet. The Figma document can be found [here][figmalink] and its corresponding prototype [here][figmaprotolink].

## What will happen to Foxtrot once the kernel is ready?
As soon as the kernel is ready to handle Foxtrot, support for running Foxtrot on any other system than Fuchsia and Dahlia Linux will be removed due to the deep integration the shell has with the underlying system. We will ensure that it will remain somewhat compatible with other systems, but we cannot ensure permanent compatibility with Android, IOS, Linux, Web, and Mac OS.

[figmalink]: https://www.figma.com/file/33OVttG4asFVEm2zb3LHfg/Foxtrot-UI?node-id=0%3A1
[figmaprotolink]: https://www.figma.com/proto/33OVttG4asFVEm2zb3LHfg/Foxtrot-UI-UX?node-id=0%3A1&scaling=contain
