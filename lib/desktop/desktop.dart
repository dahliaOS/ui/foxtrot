import 'dart:math';
import 'dart:ui';

import 'package:Pangolin/applications/calculator/calculator.dart';
import 'package:Pangolin/applications/editor/editor.dart';
import 'package:Pangolin/applications/terminal/main.dart';
import 'package:Pangolin/applications/monitor/monitor.dart';
import 'package:Pangolin/desktop/settings/settings.dart';
import 'package:Pangolin/utils/others/key_ring.dart';
import 'package:Pangolin/desktop/window/window_space.dart';
import 'package:Pangolin/utils/hiveManager.dart';
import 'package:Pangolin/utils/themes/customization_manager.dart';
import 'package:Pangolin/utils/widgets/blur.dart';
import 'package:Pangolin/utils/widgets/conditionWidget.dart';
import 'package:Pangolin/applications/files/main.dart';
import 'package:Pangolin/desktop/quicksettings/quick_settings.dart';
import 'package:flutter/material.dart';
import 'package:Pangolin/utils/widgets/system_overlay.dart';
import 'package:Pangolin/desktop/launcher/launcher_toggle.dart';
import 'package:Pangolin/desktop/quicksettings/status_tray.dart';
import 'package:Pangolin/desktop/launcher/launcher.dart';
import 'package:provider/provider.dart';
import 'package:Pangolin/utils/widgets/app_launcher.dart';
import 'package:Pangolin/utils/others/functions.dart';

import 'package:flutter/foundation.dart' show kIsWeb;

class Desktop extends StatefulWidget {
  Desktop({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DesktopState createState() => _DesktopState();
}

class _DesktopState extends State<Desktop> {
  final Tween<double> _overlayScaleTween = Tween<double>(begin: 0.9, end: 1.0);
  final Tween<double> _overlayOpacityTween =
      Tween<double>(begin: 0.0, end: 1.0);

  double distance = 0.0;

  @override
  Widget build(BuildContext context) {
    final customizationNotifier = context.watch<CustomizationNotifier>();
    final _random = new Random();
    return ChangeNotifierProvider(
      create: (_) => CustomizationNotifier(),
      child: Scaffold(
        body: Stack(
      fit: StackFit.passthrough,
      children: <Widget>[
        // 1 - Desktop background image
        Container(
          decoration: BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/Desktop/Wallpapers/Nature/mountain.jpg"),
              fit: BoxFit.cover,
            ),
          ),
        ),

        // 2 - Example usage of windows widgets
        WindowPlaygroundWidget(),
        /*Window(
              initialPosition: Offset.fromDirection(350.0,-40.0),
              initialSize: Size(355,628),
              child: Container(
                color: Colors.deepOrange[200],
              ),
              color: Colors.deepOrange
            ),
            Window(
              initialPosition: Offset.fromDirection(350.0,-40.0),
              initialSize: Size(355,628),
              child: Container(color: Colors.deepPurple[200]),
              color: Colors.deepPurple //Calculator(),
            ),*/

        // 3 - Swipe detector
        Positioned(
          left: 0.0,
          right: 0.0,
          bottom: 0.0,
          child: GestureDetector(
            key: KeyRing.swipeDetectorKey,
            behavior: HitTestBehavior.translucent,
            onVerticalDragUpdate: (event) {
              distance += event.delta.dy;
              if (distance < -50) setOverlayVisibility(
                overlay: KeyRing.launcherOverlayKey,
                visible: true,
              ); else if (distance < -(MediaQuery.of(context).size.height/2))
              setOverlayVisibility(
                overlay: KeyRing.searchRowKey,
                visible: true
              );
              //CUTFEATURE(bleonard252): set the top of the app row to the Y of the pointer
            },
            child: Container(height: 8)
          )
        ),

        // // 4 - Launcher Panel
        // SystemOverlay(
        //   key: KeyRing.launcherOverlayKey,
        //   builder: (Animation<double> animation) => Center(
        //     child: AnimatedBuilder(
        //       animation: animation,
        //       builder: (BuildContext context, Widget child) => FadeTransition(
        //         opacity: _overlayOpacityTween.animate(animation),
        //         child: SlideTransition(
        //           position: Tween(begin: Offset(0, 1), end: Offset(0, 0))
        //               .animate(animation),
        //           child: child,
        //         ),
        //       ),
        //       child: ClipRRect(
        //         //borderRadius: BorderRadius.circular(5.0),//THIS IS THE ROUNDING OF THE LAUNCHER INCASE YOU WANT IT TO CHANGE
        //         child: Container(
        //             padding: const EdgeInsets.all(0.0),
        //             alignment: Alignment.center,
        //             width: 1.7976931348623157e+308,
        //             height: 1.7976931348623157e+308,
        //             child: LauncherWidget() //Launcher(),
        //             ),
        //       ),
        //     ),
        //   ),
        //   callback: (bool visible) {
        //     KeyRing.launcherToggleKey.currentState.toggled = visible;
        //   },
        // ),

        // 4 - The bottom bar
        SystemOverlay(
          key: KeyRing.launcherOverlayKey,
          builder: (Animation<double> animation) => Stack(children: [
            AnimatedBuilder(
            animation: animation,
            builder: (BuildContext context, Widget child) => FadeTransition(
              opacity: _overlayOpacityTween.animate(animation),
              child: GestureDetector(
              behavior: HitTestBehavior.opaque,
              onTap: () => setOverlayVisibility(
                overlay: KeyRing.launcherOverlayKey,
                visible: false,
              ),
              child: Container(
                decoration: BoxDecoration(color: Color(0x7f000000)),
                alignment: Alignment.center,
              ))
            )),
            Positioned(
              left: MediaQuery.of(context).size.width > 640 ? (MediaQuery.of(context).size.width / 2) - 320 : null,
              bottom: 0.0,
              width: MediaQuery.of(context).size.width <= 640 ? MediaQuery.of(context).size.width : null,
              child: AnimatedBuilder(
              animation: animation,
              builder: (BuildContext context, Widget child) => SlideTransition(
                position: Tween(begin: Offset(0, 50), end: Offset(0, 0))
                    .animate(animation),
                child: child,
              ),
              child: Container(
                //alignment: Alignment.topCenter,
                width: 640,
                child: Column(children:[
                  Container(
                    height: 50.0,
                    // child: GestureDetector(
                    // behavior: HitTestBehavior.opaque,
                    // onTap: hideOverlays,
                    child: ClipRRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 0.0, sigmaY: 0.0),
                        child: Container(
                          //color: Color.fromARGB(150, 0, 0, 0),
                          decoration: BoxDecoration(
                            color: Colors.grey[400]
                            //uncomment below to add radius to the launcher panel
                            //borderRadius: BorderRadius.circular(100),
                            ),
                          height: 50.0,
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                            crossAxisAlignment:
                                CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: <Widget>[
                                  AppLauncherButton(
                                    app: Calculator(),
                                    icon:
                                        'assets/images/icons/v2/compiled/calculator.png',
                                    color: Colors.green,
                                    callback: toggleCallback,
                                  ),
                                  AppLauncherButton(
                                    app: TextEditorApp(),
                                    icon:
                                        'assets/images/icons/v2/compiled/notes.png',
                                    color: Colors.amber[700],
                                    callback: toggleCallback),
                                  AppLauncherButton(
                                    app: TerminalApp(),
                                    icon:
                                        'assets/images/icons/v2/compiled/terminal.png',
                                    color: Colors.grey[900],
                                    callback: toggleCallback),
                                  AppLauncherButton(
                                    app: Files(),
                                    icon:
                                        'assets/images/icons/v2/compiled/files.png',
                                    color: Colors.deepOrange,
                                    callback: toggleCallback),
                                  AppLauncherButton(
                                    app: Tasks(),
                                    icon:
                                        'assets/images/icons/v2/compiled/task.png',
                                    color: Colors.cyan[900],
                                    callback: toggleCallback,
                                  ),
                                  AppLauncherButton(
                                    app: Settings(),
                                    icon:
                                        'assets/images/icons/v2/compiled/settings.png',
                                    color: Colors.deepOrange[700],
                                    callback: toggleCallback),
                                  // AppLauncherButton(
                                  //   app: HisApp(),
                                  //   icon:
                                  //       'assets/images/icons/v2/compiled/theme.png',
                                  //   color: Colors.grey[900],
                                  //   callback: toggleCallback),
                                ]),
                            ],
                          ),
                        )
                      )
                    ),
                  ),
                  Container(
                    height: 50.0,
                    // child: GestureDetector(
                    // behavior: HitTestBehavior.opaque,
                    // onTap: hideOverlays,
                    child: ClipRRect(
                      child: BackdropFilter(
                        filter: ImageFilter.blur(
                          sigmaX: 0.0, sigmaY: 0.0),
                        child: Container(
                          //color: Color.fromARGB(150, 0, 0, 0),
                          decoration: BoxDecoration(
                            color: Colors.grey[600]
                            //uncomment below to add radius to the launcher panel
                            //borderRadius: BorderRadius.circular(100),
                            ),
                          height: 50.0,
                          padding: const EdgeInsets.all(8.0),
                          child: Row(
                            mainAxisAlignment:
                                MainAxisAlignment.spaceBetween,
                            crossAxisAlignment:
                                CrossAxisAlignment.center,
                            children: <Widget>[
                              Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                mainAxisSize: MainAxisSize.max,
                                crossAxisAlignment:
                                    CrossAxisAlignment.start,
                                children: <Widget>[
                                  FlutterLogo()
                                ]),
                            ],
                          ),
                        )
                      )
                    ),
                  )
                ])
              ),
            ))
          ])
        ),

        // WallpaperPicker(),
      ],
    ))
    );
  }
}
